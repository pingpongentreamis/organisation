# Organisation
#  Projet Cloud : Partie Organisation 

** Procedure de COMMIT **
*  [ADD] Ajout d'une fonctionalité ou de Page, Composant, module, service etc
*  [FIX] Correction d'un bugs
*  [REFACT] ré-écriture de code

** Mise en place du CRUD **
1.  Configuration du proxy :

        *  Ajouter un fichier proxyconfig.json dans /organisation-front
            `{
                "/api/*": {
                    "target": "https://127.0.0.1:443",
                    "secure": false,
                    "changeOrigin": true,
                    "pathRewrite": {
                        "^/api": "https://127.0.0.1"
                      }
                }
            }`
            
        *  Ajouter dans le fichier "package.json" dans "script" -> "start"
        `"start": "ng serve --proxy-config proxyconfig.json",`
        
2.  Mise en place du SERVICE   

        *  Creer un service DANS UN DOSSIER "SERVICES/API-CALL" avec un nom pertinent
            `ng g s [nom du service]` 
        
        * Creer un model de Données [exemple] DANS UN DOSSIER "Modele" avec un nom pertinent
            `export class JoueursModele {
                nom_joueur: string;
                prenom_joueur: string;
                nationalite_joueur: string;
            }`
        
        *l'ajouter à app.module.ts dans les providers
            `@NgModule({
              declarations: [
                AppComponent
              ],
              imports: [
                BrowserModule,
                AppRoutingModule,
                HttpClientModule
              ],
              providers: [JoueurCRUDService,
              ...`
3.  Creation d'une requête en angular
    *  GET
        *   lazy config  
        `this.http.get<TabJoueurs>('/api/joueurs')`
        *   hard config
            `this.http.get<TabJoueurs>('/api/joueurs').pipe(
                tap(_ => console.log('fetched joueurs')),
                catchError(handleError<TabJoueurs>('getJoueur')));`
    *  POST
        `this.http.post<JoueursModele>('/api/joueur/', joueur , httpOptions)`
    *  PUT
        `this.http.put<JoueursModele>('/api/joueur/' + id, joueur , httpOptions)`
    *  DELETE
        `this.http.delete<JoueursModele>('/api/joueur/' + id , httpOptions)`
        
        
        