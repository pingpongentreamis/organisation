import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './Pages/login/login.component';
import { CreateMatchComponent } from './Pages/create-match/create-match.component';
import { CreateJoueurComponent } from './Pages/create-joueur/create-joueur.component';
import { CreateArbitreComponent } from './Pages/create-arbitre/create-arbitre.component';
import { AllMatchsComponent } from './Pages/all-matchs/all-matchs.component';
import { AuthGuard } from './Services/auth.guard';


// { path: 'match', component: MatchComponent, canActivate: [AuthGuard] },
const routes: Routes = [{
  path: 'login', component: LoginComponent },
{ path: 'createMatch/:id',        component: CreateMatchComponent, canActivate: [AuthGuard] },
{ path: 'createMatch',        component: CreateMatchComponent, canActivate: [AuthGuard] },
{ path: 'createJoueur', component: CreateJoueurComponent, canActivate: [AuthGuard]},
{ path: 'createArbitre', component: CreateArbitreComponent, canActivate: [AuthGuard]},
{ path: 'allMatches', component: AllMatchsComponent, canActivate: [AuthGuard]},
{ path: '',   redirectTo: 'login', pathMatch: 'full' },
{ path: '**', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
