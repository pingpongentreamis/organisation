import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { PartieCRUDService } from 'src/app/Services/API-CALL/partie-crud.service';

@Component({
  selector: 'app-match-case',
  templateUrl: './match-case.component.html',
  styleUrls: ['./match-case.component.scss']
})
export class MatchCaseComponent implements OnInit {
  @Input() match: any;
  @Output() actu = new EventEmitter<boolean>();

  constructor(private router: Router, private partie: PartieCRUDService) { }

  ngOnInit() {
  }

  edit(id) {
    this.router.navigate(['createMatch/' + id]);
  }

  delete(id) {
    this.partie.deletePartie(id).subscribe((data) => {
      this.actu.emit();
      console.log(data);
    });
  }

}
