import { JoueurModele } from './JoueurModele';
import { SportModele } from './SportModele';
import { ArbitreModele } from './ArbitreModele';
import { OrganisationModele } from './OrganisationModele';

export class PartieModele {
    organisation: OrganisationModele;
    arbitre: ArbitreModele;
    joueur1: JoueurModele;
    joueur2: JoueurModele;
    sport: SportModele[];
    etat: string;
    DateTime: string;
}
