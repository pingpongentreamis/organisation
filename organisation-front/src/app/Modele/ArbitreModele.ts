export class ArbitreModele {
    id?:number;
    nom_arbitre: string;
    prenom_arbitre: string;
    nationalite_arbitre: string;
    login: string;
    pass:string;
}