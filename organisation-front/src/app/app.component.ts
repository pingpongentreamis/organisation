import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'organisation-front';
  navLinks: any[];
  activeLinkIndex = -1;

  constructor(private router: Router) {
    this.navLinks = [
      {
          icon: 'person_add',
          label: 'Creer Joueur',
          link: './createJoueur',
          index: 0
      }, {
          icon: 'flag',
          label: 'Creer Arbitre',
          link: './createArbitre',
          index: 1
      }, {
          icon: 'play_circle_outline',
          label: 'Creer Match',
          link: './createMatch',
          index: 2
      }, {
        icon: 'grid_on',
        label: 'Tous les matchs',
        link: './allMatches',
        index: 3
      }];
  }

  ngOnInit(): void {
    this.router.events.subscribe((res) => {
      this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab => tab.link === '.' + this.router.url));
    });
  }

}
