import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { PartieModele } from 'src/app/Modele/PartieModele';
import { PartieCRUDService } from 'src/app/Services/API-CALL/partie-crud.service';
import { JoueurCRUDService } from 'src/app/Services/API-CALL/joueur-crud.service';
import { ArbitreCRUDService } from 'src/app/Services/API-CALL/arbitre-crud.service';
import { OrganisationCrudService } from 'src/app/Services/API-CALL/organisation-crud.service';
import { SportCrudService } from 'src/app/Services/API-CALL/sport-crud.service';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { empty } from 'rxjs';

export interface Sport {
  value: string;
  viewValue: string;
}

export interface Arbitre {
  value: string;
  viewValue: string;
}

export interface Joueur {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-create-match',
  templateUrl: './create-match.component.html',
  styleUrls: ['./create-match.component.scss']
})
export class CreateMatchComponent implements OnInit {

  id;
  update = false;
  sports;
  arbitres;
  joueurs;
  organisations;
  date;

  createMatchForm = new FormGroup({
    Sport: new FormControl('', Validators.required),
    Date: new FormControl('', Validators.required),
    Arbitre: new FormControl('', Validators.required),
    Etat: new FormControl('', Validators.required),
    Organisation: new FormControl('', Validators.required),
    Joueur1: new FormControl('', Validators.required),
    Joueur2: new FormControl('', Validators.required),
  });

  constructor(private adapter: DateAdapter<any>,
              private partie: PartieCRUDService,
              private joueur: JoueurCRUDService,
              private arbitre: ArbitreCRUDService,
              private organisation: OrganisationCrudService,
              private sport: SportCrudService,
              private route: ActivatedRoute,
              private router: Router) {
    this.adapter.setLocale('fr');
  }

  ngOnInit() {

    this.joueur.getJoueur().subscribe((data) => {
      console.log(data);

      this.joueurs = data;
    });

    this.arbitre.getArbitres().subscribe((data) => {
      console.log(data);
      this.arbitres = data;
    });

    this.organisation.getOrganisations().subscribe((data) => {
      console.log(data);
      this.organisations = data;
    });

    this.sport.getSports().subscribe((data) => {
      console.log(data);
      this.sports = data;
    });

    this.id = this.route.snapshot.paramMap.get('id');

    if (this.id !== undefined && this.id !== '' && this.id !== null) {
      this.partie.getPartieById(this.id).subscribe((data) => {
        
        this.createMatchForm.get('Sport').setValue(data.sport[0].ID);
        this.date = new Date(data.DateTime);
        this.createMatchForm.get('Date').setValue(this.date);
        // console.log(date.getFullYear()+'/'+date.getMonth()+'/'+date.getDay());
        // this.createMatchForm.get('Date').setValue(date.getFullYear()+'/'+date.getMonth()+'/'+date.getDay());
        this.createMatchForm.get('Arbitre').setValue(data.arbitre[0].id);
        this.createMatchForm.get('Etat').setValue(data.etat);
        this.createMatchForm.get('Organisation').setValue(data.organisation[0].id);
        this.createMatchForm.get('Joueur1').setValue(data.joueur1.ID);
        this.createMatchForm.get('Joueur2').setValue(data.joueur2.ID);
      });
      this.update = true;
    }
  }

  formatDate(date: Date){
    //console.log(date.getFullYear()+'-'+date.getMonth()+'-'+date.getDay()+' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds());
    const tabTemps = this.createMatchForm.get('Date').value._i;
    let dateString;
    console.log(tabTemps);
    
    if (tabTemps['year'] === undefined) {
      dateString = tabTemps.split(/\//gi);
      dateString = dateString[2] + '-' + dateString[1] + '-' + dateString[0];
    } else {
      dateString = tabTemps['year'] + '-' + (Number(tabTemps['month'])+1) + '-' + tabTemps['date'];
    }
    console.log(dateString);
    return dateString+' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds()
  }
  onSubmit() {
    console.log(this.createMatchForm.get('Date').value);
    let date = new Date(this.createMatchForm.get('Date').value._d)
    if (this.createMatchForm.invalid) {
      return;
    }
    if (this.createMatchForm.get('Joueur1').value === this.createMatchForm.get('Joueur2').value){
      return;
    }
  
    
    let partie: PartieModele  = new PartieModele();
    partie.arbitre = this.createMatchForm.get('Arbitre').value;
    partie.DateTime  = this.formatDate(date);
    partie.joueur1 = this.createMatchForm.get('Joueur1').value;
    partie.joueur2 = this.createMatchForm.get('Joueur2').value;
    partie.sport = this.createMatchForm.get('Sport').value;
    partie.etat = this.createMatchForm.get('Etat').value;
    partie.organisation = this.createMatchForm.get('Organisation').value;

    if (this.update) { 
      this.partie.updatePartie(this.id, partie).subscribe((data) => {
        console.log(data);});
    } else {
      this.partie.insertPartie(partie).subscribe((data)=>{console.log(data);});  
    }
    this.router.navigate(['allMatches']);

  }

}
