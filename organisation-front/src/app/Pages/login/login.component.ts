import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/Services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  hide = true;
  
  loginForm = new FormGroup({
    Login: new FormControl('', Validators.required),
    MotDePasse: new FormControl('', Validators.required)
  });

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {

    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }
    this.auth.getUserForlogin(this.loginForm.get('Login').value, this.loginForm.get('MotDePasse').value).subscribe(
      (data) => {
        if (data === undefined) { return; }
        console.log(data);
        this.auth.setLoggedIn(data.password);

        this.router.navigate(['createJoueur']);
        });
}

}
