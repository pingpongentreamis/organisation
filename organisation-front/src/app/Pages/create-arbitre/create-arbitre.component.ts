import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ArbitreCRUDService } from 'src/app/Services/API-CALL/arbitre-crud.service';
import { ArbitreModele } from 'src/app/Modele/ArbitreModele';

export interface Nationalite {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-create-arbitre',
  templateUrl: './create-arbitre.component.html',
  styleUrls: ['./create-arbitre.component.scss']
})
export class CreateArbitreComponent implements OnInit {

  nationalites: Nationalite[] = [
    {value: '1', viewValue: 'France'},
    {value: '2', viewValue: 'Union Sovietique'},
    {value: '3', viewValue: 'Corée du Nord'}
  ];

  createArbitreForm = new FormGroup({
    Nom: new FormControl('', Validators.required),
    Prenom: new FormControl('', Validators.required),
    Nationalite: new FormControl('', Validators.required),
    Login: new FormControl('', Validators.required),
    MotDePasse: new FormControl('', Validators.required)
  });

  constructor(private arbitre: ArbitreCRUDService) { }

  ngOnInit() {
  }

  onSubmit() {
    // stop here if form is invalid
    if (this.createArbitreForm.invalid) {
         return;
    }

    let arbitre: ArbitreModele = new ArbitreModele();
    arbitre.login = this.createArbitreForm.get('Login').value;
    arbitre.prenom_arbitre = this.createArbitreForm.get('Prenom').value;
    arbitre.nom_arbitre = this.createArbitreForm.get('Nom').value;
    arbitre.nationalite_arbitre = this.createArbitreForm.get('Nationalite').value;
    arbitre.pass = this.createArbitreForm.get('MotDePasse').value;

    this.arbitre.insertAbitre(arbitre).subscribe();
  }
}
