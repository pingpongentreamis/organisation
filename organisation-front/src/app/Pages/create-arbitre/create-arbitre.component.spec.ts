import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateArbitreComponent } from './create-arbitre.component';

describe('CreateArbitreComponent', () => {
  let component: CreateArbitreComponent;
  let fixture: ComponentFixture<CreateArbitreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateArbitreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateArbitreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
