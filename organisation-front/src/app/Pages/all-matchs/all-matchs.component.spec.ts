import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllMatchsComponent } from './all-matchs.component';

describe('AllMatchsComponent', () => {
  let component: AllMatchsComponent;
  let fixture: ComponentFixture<AllMatchsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllMatchsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllMatchsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
