import { Component, OnInit } from '@angular/core';
import { PartieCRUDService } from 'src/app/Services/API-CALL/partie-crud.service';

@Component({
  selector: 'app-all-matchs',
  templateUrl: './all-matchs.component.html',
  styleUrls: ['./all-matchs.component.scss']
})
export class AllMatchsComponent implements OnInit {
  matchs = [];

  constructor(private partie: PartieCRUDService) { }

  ngOnInit() {
    this.setData();
  }

  setData() {
    this.partie.getPartie().subscribe( (data) => {
      console.log(data);
      this.matchs = data;
    });
  }

  onActu(agreed: boolean) {
    console.log('APPEL O');
    this.setData();

  }

}
