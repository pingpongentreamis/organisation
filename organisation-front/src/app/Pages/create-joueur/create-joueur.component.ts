import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { JoueurCRUDService } from 'src/app/Services/API-CALL/joueur-crud.service';
import { JoueurModele } from 'src/app/Modele/JoueurModele';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

export interface Nationalite {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-create-joueur',
  templateUrl: './create-joueur.component.html',
  styleUrls: ['./create-joueur.component.scss']
})
export class CreateJoueurComponent implements OnInit {

  nationalites: Nationalite[] = [
    {value: '1', viewValue: 'France'},
    {value: '2', viewValue: 'Union Sovietique'},
    {value: '3', viewValue: 'Corée du Nord'}
  ];

  createJoueurForm = new FormGroup({
    Nom: new FormControl(''),
    Prenom: new FormControl(''),
    Nationalite: new FormControl(''),
    DateDeNaissance: new FormControl('')
  });

  constructor(private adapter: DateAdapter<any>, private joueur: JoueurCRUDService) {
    this.adapter.setLocale('fr');
  }

  ngOnInit() {
  }

  onSubmit() {
    if (this.createJoueurForm.invalid) {
      return;
    }
    let joueur: JoueurModele = new JoueurModele();
    joueur.Nom = this.createJoueurForm.get('Nom').value;
    joueur.Prenom = this.createJoueurForm.get('Prenom').value;
    joueur.Nationalite = this.createJoueurForm.get('Nationalite').value;

    this.joueur.insertJoueur(joueur).subscribe();


}
}
