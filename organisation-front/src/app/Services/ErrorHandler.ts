import { ErrorHandler, Injectable} from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
@Injectable()
export class ErrorsHandler implements ErrorHandler {
  handleError(error: any) {
    if (error instanceof HttpErrorResponse) {
        // Server or connection error happened
        if (!navigator.onLine) {
            // Handle offline error
            console.error('It happens: ', error);
        } else {
            // Handle Http Error (error.status === 403, 404...)
            console.error('It happens: ', error);
        }
    } else {
        // Handle Client Error (Angular Error, ReferenceError...)
        console.error('It happens: ', error);
    }
    // Log the error anyway
    console.error('It happens: ', error);
    }
}