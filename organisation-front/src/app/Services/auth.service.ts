import { Injectable } from '@angular/core';
import { OrganisationCrudService } from './API-CALL/organisation-crud.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loggedInStatus = false;

  constructor(private organisation: OrganisationCrudService, ) { }

  setLoggedIn(value: boolean) {
    this.loggedInStatus = value
  }

  get isLoggedIn() {
    return this.loggedInStatus
  }

  getUserForlogin(nom: string, pass: string) {
    return this.organisation.getOrganisationForlogin(nom, pass);
  }
}
