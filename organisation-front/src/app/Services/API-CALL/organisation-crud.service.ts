import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { handleError } from '../../Function-Utils/LogError';
import { OrganisationModele } from '../../Modele/OrganisationModele';
import { environment } from 'src/environments/environment';


interface TabOrganisation{
  organisation: Array<OrganisationModele>;
}

interface response {
  code: string;
  password: boolean;
  login: boolean;
  statut: string;
  idOrganisation: number
}
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': 'https://localhost:443' }),
  withCredentials: true
};

@Injectable({
  providedIn: 'root'
})
export class OrganisationCrudService {
  private url = environment.url;
  constructor(private http: HttpClient) { }

    /**
 * getOrganisations
 * Recupère tous les Organisation en base de données
 */
getOrganisations(): Observable<TabOrganisation> {
  return  this.http.get<TabOrganisation>(this.url+'/Organisations').pipe(
    tap(_ => console.log('fetched Organisations')),
    catchError(handleError<TabOrganisation>('getOrganisations')));
}

/**
 * getOrganisationById
 * Recupère un Organisation avec sur id
 * @param id - id of the Organisation
 */
getOrganisationById(id: number): Observable<OrganisationModele> {
  return  this.http.get<OrganisationModele>(this.url+'/Organisation/' + id).pipe(
    tap(_ => console.log(`fetched Organisation id=${id}`)),
    catchError(handleError<OrganisationModele>(`getAbitreById id=${id}`))
  );
}

/**
 * getOrganisationForlogin
 * Login de l'Organisation
 * @param nom - nom of the Organisation
 */
getOrganisationForlogin(login: string, pass: string): Observable<response> {
  return  this.http.post<response>(this.url+'/OrganisationLogin', {'login': login, 'pass': pass }, httpOptions).pipe(
    tap(_ => console.log(`fetched Organisation nom=${login}`)),
    catchError(handleError<response>(`getAbitreById nom=${login}`))
  );
}
}
