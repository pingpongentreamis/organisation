import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { handleError } from '../../Function-Utils/LogError';
import { SportModele } from '../../Modele/SportModele';
import { environment } from 'src/environments/environment';

interface TabSport {
  sport: Array<SportModele>;
}

interface response {
  code: string;
  password: boolean;
  login: boolean;
  statut: string;
  idSport: number
}
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': 'https://localhost:443' }),
  withCredentials: true
};

@Injectable({
  providedIn: 'root'
})
export class SportCrudService {
  private url = environment.url;
  constructor(private http:HttpClient) { }

  /**
 * getSports
 * Recupère tous les Sport en base de données
 */
getSports(): Observable<TabSport> {
  return  this.http.get<TabSport>(this.url+'/Sports').pipe(
    tap(_ => console.log('fetched Sports')),
    catchError(handleError<TabSport>('getSports')));
}

/**
 * getSportById
 * Recupère un Sport avec sur id
 * @param id - id of the Sport
 */
getSportById(id: number): Observable<SportModele> {
  return  this.http.get<SportModele>(this.url+'/Sport' + id).pipe(
    tap(_ => console.log(`fetched Sport id=${id}`)),
    catchError(handleError<SportModele>(`getAbitreById id=${id}`))
  );
}

}
