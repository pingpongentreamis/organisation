import { TestBed } from '@angular/core/testing';

import { SportCrudService } from './sport-crud.service';

describe('SportCrudService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SportCrudService = TestBed.get(SportCrudService);
    expect(service).toBeTruthy();
  });
});
