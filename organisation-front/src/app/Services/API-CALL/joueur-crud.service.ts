import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { handleError } from '../../Function-Utils/LogError';
import { JoueurModele } from '../../Modele/JoueurModele';
import { environment } from 'src/environments/environment';

interface TabJoueurs {
  joueurs: Array<JoueurModele>;
}

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': 'https://localhost:443' }),
  withCredentials: true
};

@Injectable({
  providedIn: 'root'
})
export class JoueurCRUDService {
  private url = environment.url;
  constructor(private http: HttpClient) { }

/**
 * getJoueur
 * Recupère tous les joueurs en base de données
 */
getJoueur(): Observable<TabJoueurs> {
  return  this.http.get<TabJoueurs>(this.url+'/joueurs').pipe(
    tap(_ => console.log('fetched joueurs')),
    catchError(handleError<TabJoueurs>('getJoueur')));
}

/**
 * getJoueurById
 * Recupère un joueur avec sur id
 * @param id - id of the player 
 */
getJoueurById(id: number): Observable<JoueurModele> {
  return  this.http.get<JoueurModele>(this.url+'/joueur/' + id).pipe(
    tap(_ => console.log(`fetched hero id=${id}`)),
    catchError(handleError<JoueurModele>(`getJoueurById id=${id}`))
  );
}

/**
 * insertJoueur
 * Insert a joueur en base de sonnées
 * @param joueur - object player
 */
insertJoueur(joueur: JoueurModele) {
  return this.http.post<JoueurModele>(this.url+'/joueur/', joueur , httpOptions).pipe(
    tap((joueur: JoueurModele) => console.log(`added joueur`)),
    catchError(handleError<JoueurModele>('addHero'))
  );
}

/**
 * modifyJoueur
 * Update a joueur en base de sonnées
 * @param id - id of the player
 * @param joueur - object player
 */
updateJoueur(id: number, joueur: JoueurModele) {
return this.http.put<JoueurModele>(this.url+'/joueur/' + id, joueur , httpOptions).pipe(
  tap((joueur: JoueurModele) => console.log(`update joueur`, id, joueur)),
  catchError(handleError<JoueurModele>('addHero'))
);
}

/**
 * deleteJoueur
 * Suprime un joueur en base de sonnées
 * @param id - id of the player
 */
deleteJoueur(id: number) {
return this.http.delete<JoueurModele>(this.url+'/joueur/' + id , httpOptions).pipe(
  tap((joueur: JoueurModele) => console.log(`delete joueur`, id, joueur)),
  catchError(handleError<JoueurModele>('addHero'))
);
}
}
