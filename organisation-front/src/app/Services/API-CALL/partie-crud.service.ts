import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { handleError } from '../../Function-Utils/LogError';
import { PartieModele } from '../../Modele/PartieModele';
import { environment } from 'src/environments/environment';

interface TabPartie {
  partie: Array<PartieModele>;
}

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': 'https://localhost:443' }),
  withCredentials: true
};

@Injectable({
  providedIn: 'root'
})
export class PartieCRUDService {
  private url = environment.url;
  constructor(private http: HttpClient) { }

/**
 * getPartie
 */
getPartie(): Observable<any> {
  return  this.http.get<any>(this.url+'/Parties/').pipe(
    tap(_ => console.log(`fetched Parties `)),
    catchError(handleError<any>(`getPartie`))
  );
}

/**
 * getPartieById
 * Recupère une partie avec un id
 * @param id - id of the Parties
 */
getPartieById(id: number): Observable<PartieModele> {
  return  this.http.get<PartieModele>(this.url+'/Partie/' + id).pipe(
    tap(_ => console.log(`fetched partie id=${id}`)),
    catchError(handleError<PartieModele>(`getPartieById id=${id}`))
  );
}

/**
 * insertPartie
 * Insert une partie en base de données
 * @param partie - object partie
 */
insertPartie(partie: PartieModele) {
  return this.http.post<PartieModele>(this.url+'/Partie', partie , httpOptions).pipe(
    tap((partie: PartieModele) => console.log(`added partie`)),
    catchError(handleError<PartieModele>('insert partie'))
  );
}

/**
 * updatePartie
 * Update a partie en base de données
 * @param id - id partie
 * @param partie - object partie
 */
updatePartie(id: number, partie: PartieModele) {
return this.http.put<PartieModele>(this.url+'/Partie/' + id, partie , httpOptions).pipe(
  tap((partie: PartieModele) => console.log(`update partie`, id, partie)),
  catchError(handleError<PartieModele>('update partie'))
);
}

/**
 * deletePartie
 * Suprime une partie en base de données
 * @param id - id of the partie
 */
deletePartie(id: number) {
return this.http.delete<PartieModele>(this.url+'/Partie/' + id , httpOptions).pipe(
  tap((partie: PartieModele) => console.log(`delete Partie`, id, partie)),
  catchError(handleError<PartieModele>('delete   Partie'))
);
}
}
