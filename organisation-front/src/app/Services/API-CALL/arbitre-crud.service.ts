import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { handleError } from '../../Function-Utils/LogError';
import { ArbitreModele } from '../../Modele/ArbitreModele';
import { environment } from 'src/environments/environment';

interface TabArbitres {
  arbitre: Array<ArbitreModele>;
}

interface response {
  code: string;
  password: boolean;
  login: boolean;
  statut: string;
  idArbitre: number
}
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': 'https://localhost:443' }),
  withCredentials: true
};

@Injectable({
  providedIn: 'root'
})
export class ArbitreCRUDService {
  private url = environment.url;

  constructor(private http: HttpClient) { }

  /**
 * getArbitres
 * Recupère tous les Arbitre en base de données
 */
getArbitres(): Observable<TabArbitres> {
  return  this.http.get<TabArbitres>(this.url+'/Arbitres').pipe(
    tap(_ => console.log('fetched Arbitres')),
    catchError(handleError<TabArbitres>('getArbitres')));
}

/**
 * getArbitreById
 * Recupère un arbitre avec sur id
 * @param id - id of the arbitre
 */
getArbitreById(id: number): Observable<ArbitreModele> {
  return  this.http.get<ArbitreModele>(this.url+'/Arbitre/' + id).pipe(
    tap(_ => console.log(`fetched arbitre id=${id}`)),
    catchError(handleError<ArbitreModele>(`getAbitreById id=${id}`))
  );
}

/**
 * getArbitreForlogin
 * Login de l'arbitre
 * @param nom - nom of the arbitre
 */
getArbitreForlogin(login: string, pass: string): Observable<response> {
  return  this.http.post<response>(this.url+'/ArbitresLogin', {'login': login, 'pass': pass }, httpOptions).pipe(
    tap(_ => console.log(`fetched arbitre nom=${login}`)),
    catchError(handleError<response>(`getAbitreById nom=${login}`))
  );
}

/**
 * insertAbitre
 * Insert un arbitre en base de données
 * @param abitre - object arbitre
 */
insertAbitre(abitre: ArbitreModele) {
  return this.http.post<ArbitreModele>(this.url + '/Arbitre', abitre , httpOptions).pipe(
    tap((arbitre: ArbitreModele) => console.log(`added arbitre`)),
    catchError(handleError<ArbitreModele>('insert arbitre'))
  );
}

/**
 * updateArbitre
 * Update a arbitre en base de données
 * @param id - id arbitre
 * @param arbitre - object arbitre
 */
updateArbitre(id: number, abitre: ArbitreModele) {
return this.http.put<ArbitreModele>(this.url+'/Abitre/' + id, abitre , httpOptions).pipe(
  tap((abitre: ArbitreModele) => console.log(`update arbitre`, id, abitre)),
  catchError(handleError<ArbitreModele>('update arbitre'))
);
}

/**
 * deleteArbitre
 * Suprime un arbitre en base de données
 * @param id - id of the arbitre
 */
deleteArbitre(id: number) {
return this.http.delete<ArbitreModele>(this.url+'/Arbitre/' + id , httpOptions).pipe(
  tap((arbitre: ArbitreModele) => console.log(`delete Arbitre`, id, arbitre)),
  catchError(handleError<ArbitreModele>('delete   arbitre'))
);
}
}
