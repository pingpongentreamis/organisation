import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
// import { Joueur } from './Modele/Joueurs';
import { ErrorsHandler } from './Services/ErrorHandler';

//Reactive forms module
import { ReactiveFormsModule } from '@angular/forms';


//Material
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {MatTabsModule} from '@angular/material/tabs';

//Composants
import {BanniereComponent} from './Components/banniere/banniere.component';
import {LoginComponent} from './Pages/login/login.component';
import { CreateMatchComponent } from './Pages/create-match/create-match.component';
import { CreateJoueurComponent } from './Pages/create-joueur/create-joueur.component';
import { CreateArbitreComponent } from './Pages/create-arbitre/create-arbitre.component';
import { AllMatchsComponent } from './Pages/all-matchs/all-matchs.component';


//Services
import { ArbitreCRUDService } from './Services/API-CALL/arbitre-crud.service';
import { JoueurCRUDService } from './Services/API-CALL/joueur-crud.service';
import { OrganisationCrudService } from './Services/API-CALL/organisation-crud.service';
import { PartieCRUDService} from './Services/API-CALL/partie-crud.service';
import {  PointCRUDService} from './Services/API-CALL/point-crud.service';
import { SportCrudService } from './Services/API-CALL/sport-crud.service';
import { AuthGuard } from './Services/auth.guard';
import { AuthService } from './Services/auth.service';
import { MatchCaseComponent } from './Components/match-case/match-case.component';
import { AddCaseComponent } from './Components/add-case/add-case.component';


@NgModule({
  declarations: [
    AppComponent,
    BanniereComponent,
    LoginComponent,
    CreateMatchComponent,
    CreateJoueurComponent,
    CreateArbitreComponent,
    AllMatchsComponent,
    MatchCaseComponent,
    AddCaseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatDatepickerModule,
    MatMomentDateModule,
    ReactiveFormsModule,
    MatTabsModule
  ],
  providers: [JoueurCRUDService,
    ArbitreCRUDService,
    OrganisationCrudService,
    PartieCRUDService,
    PointCRUDService,
    SportCrudService,
    AuthGuard,
    AuthService,
    {
      provide: ErrorsHandler,
      useClass: ErrorsHandler,
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
